Spack
======

A role to install Spack from https://github.com/spack/spack

Like it is documented here:
https://spack.io/

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)

[Dependencie Variables](vars/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/playbook.yml)


License
-------

[License](LICENSE)
